## Agencia de Viajes (Prueba de Conocimientos)

<p align="center">
<a href="https://packagist.org/packages/laravel/framework" title="Laravel"> Laravel <img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/twitter/bootstrap" title="Bootstrap">Bootstrap <img src="https://poser.pugx.org/twitter/bootstrap/v/stable" alt="License"></a>
</p>

## About Agencia de Viajes

Agencia de Viajes, permite un control de los procesos de reservación de viajes, como el registro de los viajeros (clientes), de los viajes, y las respectivas reservaciones en los mismos. Está desarrollado utilizando el framework PHP Laravel en su  versión 5 para el backEnd, y emplea CSS3, Bootstrap 4 y jQuery para la gestión del frontEnd.

Fue desarrollado como requisito o prueba de conocimientos para la postulación al cargo de Programador Full Stack.


## License

Agencia de Viajes is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
