@extends('layout.main') @section('content')
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
@endif
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>{{trans('file.add')}} {{trans('file.customer')}}</h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                        {!! Form::open(['route' => 'clientes.store', 'method' => 'post']) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ ($errors->has('cedula'))?'error':'' }}">
                                    <label><strong>Cédula *</strong> </label>
                                    <input type="text" name="cedula" required class="form-control">
                                    @if($errors->has('cedula'))
                                    <label class="help-block">{{ $errors->first('cedula') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ ($errors->has('nombre'))?'error':'' }}">
                                    <label><strong>{{trans('file.name')}} *</strong> </label>
                                    <input type="text" name="nombre" required class="form-control">
                                    @if($errors->has('nombre'))
                                    <label class="help-block">{{ $errors->first('nombre') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ ($errors->has('telefono'))?'error':'' }}">
                                    <label><strong>{{trans('file.Phone Number')}} *</strong></label>
                                    <input type="text" name="telefono" required class="form-control">
                                    @if($errors->has('telefono'))
                                    <label class="help-block">{{ $errors->first('telefono') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ ($errors->has('direccion'))?'error':'' }}">
                                    <label><strong>{{trans('file.Address')}} *</strong></label>
                                    <input type="text" name="direccion" required class="form-control">
                                    @if($errors->has('direccion'))
                                    <label class="help-block">{{ $errors->first('direccion') }}</label>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="pos" value="0">
                                    <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection