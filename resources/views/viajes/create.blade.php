@extends('layout.main')

@section('content')
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>{{trans('file.add')}} Viaje</h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                        {!! Form::open(['route' => 'viajes.store', 'method' => 'post', 'id' => 'viaje-form']) !!}
                        <div class="row">
                            <div class="col-md-6">
                        		<div class="form-group {{ ($errors->has('codigo'))?'error':'' }}">
                        			<label><strong>{{trans('file.viaje')}} {{trans('file.Code')}} *</strong> </label>
                        			<div class="input-group">
		                                <input type="text" name="codigo" required class="form-control">
		                                <div class="input-group-append">
				                            <button id="genbutton" type="button" class="btn btn-default">{{trans('file.Generate')}}</button>
				                        </div>
                        			</div>
                                    @if($errors->has('codigo'))
                                    <label class="help-block">{{ $errors->first('codigo') }}
                                    </label>
                                    @endif
	                            </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label><strong>Núm. plazas *</strong> </label>
                                    <input type="number" name="num_plazas" required class="form-control" min="1" step="any" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label><strong>Destino *</strong> </label>
                                    <input type="text" name="destino" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label><strong>Origen *</strong> </label>
                                    <input type="text" name="origen" required class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong>{{trans('file.Price')}} *</strong> </label>
                                    <input type="number" name="precio" required class="form-control" step="any">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="precio" value="0.00">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

	$('#genbutton').on("click", function(){
      $.get('gencode', function(data){
        $("input[name='codigo']").val(data);
      });
    });
</script>
@endsection
