@extends('layout.main') @section('content')
@if(session()->has('create_message'))
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('create_message') }}</div>
@endif
@if(session()->has('edit_message'))
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('edit_message') }}</div>
@endif
@if(session()->has('import_message'))
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('import_message') }}</div>
@endif
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
@endif
@if(session()->has('message'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div>
@endif

<section>
    <div class="container-fluid">
        <a href="{{route('viajes.create')}}" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Viaje</a>
    </div>
    <div class="table-responsive">
        <table id="product-data-table" class="table table-hover">
            <thead>
                <tr>
                    <th class="not-exported"></th>
                    <th>{{trans('file.Code')}}</th>
                    <th>Núm. Plazas</th>
                    <th>Destino</th>
                    <th>Origen</th>
                    <th>{{trans('file.Price')}}</th>
                    <th class="not-exported">{{trans('file.action')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($viajes_all as $key => $viaje)
                <tr class="viaje-link" data-viaje='[ "{{$viaje->id}}", "{{$viaje->codigo}}", "{{$viaje->num_plazas}}", "{{$viaje->destino}}", "{{$viaje->origen}}", "{{$general_setting->currency}} {{$viaje->precio}}"]'>
                    <td>{{$key}}</td>
                    <td>{{ $viaje->codigo }}</td>
                    <td>{{ $viaje->num_plazas}}</td>
                    <td>{{ $viaje->destino}}</td>
                    <td>{{ $viaje->origen}}</td>
                    <td>{{$general_setting->currency}} {{ number_format($viaje->precio,2,',','.')}}</td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{trans('file.action')}}
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu">
                              <li>
                                <a href="{{ route('viajes.edit', ['id' => $viaje->id]) }}" class="btn btn-link"><i class="fa fa-edit"></i> {{trans('file.edit')}}</a>
                              </li>
                                <li class="divider"></li>
                                {{ Form::open(['route' => ['viajes.destroy', $viaje->id], 'method' => 'DELETE'] ) }}
                                <li>
                                    <button type="submit" class="btn btn-link" onclick="return confirmDelete()"><i class="fa fa-trash"></i> {{trans('file.delete')}}</button>
                                </li>
                                {{ Form::close() }}
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</section>

<script type="text/javascript">

	function confirmDelete() {
	    if (confirm("¿Seguro quiere eliminar este viaje?")) {
	        return true;
	    }
	    return false;
	}

    $('#product-data-table').DataTable( {
        "order": [],
        "language": {
            "url": "{{url('public/vendor/datatable/dataTables.spanish.json')}}"
        },
        'columnDefs': [
            {
                "orderable": false,
                'targets': [0, 6]
            },
            {
                'checkboxes': {
                   'selectRow': true
                },
                'targets': 0
            }
        ],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        dom: 'lftipr'
    } );

</script>
@endsection