@extends('layout.main') @section('content')
@if(session()->has('create_message'))
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('create_message') }}</div>
@endif
@if(session()->has('edit_message'))
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('edit_message') }}</div>
@endif
@if(session()->has('import_message'))
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('import_message') }}</div>
@endif
@if(session()->has('not_permitted'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('not_permitted') }}</div>
@endif
@if(session()->has('message'))
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{{ session()->get('message') }}</div>
@endif
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4>{{trans('file.add')}} Reservación</h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                        {!! Form::open(['route' => 'viajes.booking', 'method' => 'post', 'class' => 'payment-form']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong>{{trans('file.reference')}} No</strong></label>
                                            <p><strong>{{'sr-' . date("Ymd") . '-'. date("his")}}</strong></p>
                                        </div>
                                    </div>
                                    <input type="hidden" name="reference" value="{{'sr-' . date('Ymd') . '-'. date('his')}}">
                                    <input type="hidden" id="validate_booking" value="">
                                    <input type="hidden" id="numplazas" value="0">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><strong>{{trans('file.customer')}} *</strong></label>
                                        <div class="input-group pos">
                                            <select required name="cliente_id" id="cliente_id" class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Seleccione Cliente">
                                            @foreach($customer_list as $customer)
                                                <option value="{{$customer->id}}">{{ $customer->nombre . ' ('. $customer->cedula.') ' }}</option>
                                            @endforeach
                                            </select>
                                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addCustomer"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label><strong>{{trans('file.viaje')}} *</strong></label>
                                            <select required name="viaje_id" id="viaje_id" class="selectpicker form-control" data-live-search="true" data-live-search-style="begins" title="Seleccione Viaje">
                                                @foreach($viajes_list as $viaje)
                                                <?php $deposit[$viaje->id] = $viaje->deposit - $viaje->expense; ?>
                                                <option value="{{$viaje->id}}">{{ $viaje->origen .' - '. $viaje->destino . ' ('.$viaje->codigo.') ' }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label><strong>Número de Plazas disponibles</strong></label>
                                            <div id="numplazas_str"><strong>0</strong></div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="submit" value="Agregar" class="btn btn-primary btn-block" id="submit-button">
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="card-header d-flex align-items-center">
                        <h4>Reservación</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive mt-3">
                                    <table id="myTable" class="table table-hover order-list">
                                        <thead>
                                            <tr>
                                                <th>Cédula</th>
                                                <th>{{trans('file.name')}}</th>
                                                <th>{{trans('file.Phone Number')}}</th>
                                                <th>{{trans('file.viaje')}}</th>
                                                <th>{{trans('file.Price')}}</th>
                                                <th>{{trans('file.Subtotal')}}</th>
                                                <th><i class="fa fa-trash"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot class="tfoot active">
                                            <th colspan="5">{{trans('file.Total')}}</th>
                                            <th id="total">0.00</th>
                                            <th><i class="fa fa-trash"></i></th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- add customer modal -->
        <div id="addCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
            <div role="document" class="modal-dialog">
              <div class="modal-content">
                {!! Form::open(['route' => 'clientes.store', 'method' => 'post']) !!}
                <div class="modal-header">
                  <h5 id="exampleModalLabel" class="modal-title">{{trans('file.add')}} {{trans('file.customer')}}</h5>
                  <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                  <p class="italic"><small>{{trans('file.The field labels marked with * are required input fields')}}.</small></p>
                  <div class="form-group">
                        <label><strong>Cédula *</strong> </label>
                        <input type="text" name="cedula" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label><strong>{{trans('file.name')}} *</strong> </label>
                        <input type="text" name="nombre" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label><strong>{{trans('file.Phone Number')}} *</strong></label>
                        <input type="text" name="telefono" required class="form-control">
                    </div>
                    <div class="form-group">
                        <label><strong>{{trans('file.Address')}} *</strong></label>
                        <input type="text" name="direccion" required class="form-control">
                    </div>
                    <div class="form-group">
                    <input type="hidden" name="pos" value="1">
                      <input type="submit" value="{{trans('file.submit')}}" class="btn btn-primary">
                    </div>
                </div>
                {{ Form::close() }}
              </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

$('.selectpicker').selectpicker({
    style: 'btn-link',
});

$('select[name="cliente_id"]').on('change', function() {
    validateBooking();
});

$('select[name="viaje_id"]').on('change', function() {
    var id = $(this).val();
    getNumPlazas(id);
    updateBooking(id);
    validateBooking();
});

/**
 * Obtener el numero de plazas disponibles
 */
function getNumPlazas(id){
    $.ajax({
        type: 'GET',
        url: 'getplazas/' + id,
        success: function(data) {
            $('#numplazas_str').find('strong').text(data);
            $('#numplazas').val(data);
        }
    });
}

/**
 * Actualizar el listado de reservaciones
 */
function updateBooking(id){
    $.ajax({
        type: 'GET',
        url: 'getbooking/' + id,
        success: function(data) {
            $('#myTable').html(data);
        }
    });
}

/**
 * verificar si el cliente ya tiene reservacion registrada para este viaje
 */
function validateBooking(){
    if ($('#cliente_id').val()!='' && $('#viaje_id').val()!='') {
        $.ajax({
            type: 'POST',
            async: false,
            url: 'validatebooking',
            dataType: 'json',
            data: $('.payment-form').serialize(),
            success: function(data) {
                $('#validate_booking').val(data.valid)
            }
        });
    }
}


/**
 * Enviar formulario
 */
$('.payment-form').on('submit',function(e){

    if ($('#numplazas').val()=='0') {
        alert('Las plazas para este viaje están totalmente ocupadas.');
        e.preventDefault();
        return false;
    }

    if ($('#validate_booking').val()=='Y') {
        if (confirm('¿Seguro desea añadir esta reservación?')) {
            // enviar formulario
            return true;
        } else {
            e.preventDefault();
            return false;
        }
    } else {
        alert('Este cliente ya se encuentra registrado para este viaje');
        e.preventDefault();
        return false;
    }

});


</script>
@endsection @section('scripts')

@endsection