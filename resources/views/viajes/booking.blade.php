<thead>
    <tr>
        <th>Cédula</th>
        <th>{{trans('file.name')}}</th>
        <th>{{trans('file.Phone Number')}}</th>
        <th>{{trans('file.viaje')}}</th>
        <th>{{trans('file.Price')}}</th>
        <th>{{trans('file.Subtotal')}}</th>
        <th><i class="fa fa-trash"></i></th>
    </tr>
</thead>
<tbody>
<?php $total=0; foreach ($booking_data as $booking) { ?>
    <tr>
        <td>{{ $booking->cedula }}</td>
        <td>{{ $booking->nombre }}</td>
        <td>{{ $booking->telefono }}</td>
        <td>{{ $booking->origen.' - '.$booking->destino }}</td>
        <td>{{$general_setting->currency}} {{ number_format($booking->precio,2,',','.') }}</td>
        <td>{{$general_setting->currency}} {{ number_format($booking->precio,2,',','.') }}</td>
        <td><a href="{{ route('reservacion.destroy', ['id' => $booking->reserva_id]) }}" onclick="return confirm('¿Seguro desea eliminar esta reservación?');"><i class="fa fa-trash"></i></a></td>
        <?php $total += $booking->precio; ?>
    </tr>
<?php } ?>
</tbody>
<tfoot class="tfoot active">
    <th colspan="5">{{trans('file.Total')}}</th>
    <th id="total">{{$general_setting->currency}} {{ number_format($total,2,',','.') }}</th>
    <th><i class="fa fa-trash"></i></th>
</tfoot>