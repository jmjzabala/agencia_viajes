<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => 'auth'], function() {
	Route::get('/dashboard', function () {
		if(Auth::user()->is_deleted){
	        auth()->logout();
	        return redirect('/login')->with('delete_message', 'Your ID is no longer available');
	    }
	    else
	    	return view('home');
	});
});

Route::group(['middleware' => ['auth', 'active']], function() {

	Route::get('/', 'ViajesController@reservar');
	Route::get('/home', 'HomeController@index')->name('home');

	Route::resource('viajes', 'ViajesController');
	Route::get('viajes/gencode', 'ViajesController@generateCode');
	Route::get('reservaciones', 'ViajesController@reservar');
	Route::post('viajes/booking', 'ViajesController@booking')->name('viajes.booking');
	Route::get('getbooking/{id}', 'ViajesController@getBooking')->name('viajes.getbooking');
	Route::get('getplazas/{id}', 'ViajesController@getPlazas');
	Route::get('reservacion/{id}', 'ViajesController@bookingDestroy')->name('reservacion.destroy');
	Route::post('validatebooking', 'ViajesController@validateBooking');

	Route::resource('clientes', 'ClientesController');

});

