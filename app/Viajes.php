<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viajes extends Model
{
    protected $fillable =[
        "codigo", "num_plazas", "destino", "origen", "precio"
    ];
}
