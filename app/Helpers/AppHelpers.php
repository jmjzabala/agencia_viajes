<?php

/**
 * numberFormat
 *
 * Muestra un valor numerico con punto para miles y coma para decimales
 *
 * @access  public
 * @param   integer value
 * @return  html
 */

if (! function_exists('numberFormat')) {
    function numberFormat($value, $decimal = 2)
    {
        return number_format($value, $decimal, ',', '.');
    }
}
