<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservaciones extends Model
{
	const UPDATED_AT = null;

    protected $fillable =[
        "reference","cliente_id", "viaje_id","created_at"
    ];
}
