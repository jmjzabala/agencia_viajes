<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientesGroup;
use App\Cliente;
use App\User;
use Illuminate\Validation\Rule;
use Auth;

class ClientesController extends Controller
{
    public function index()
    {
        $customers = Cliente::get();
        return view('clientes.index', compact('customers'));
    }

    public function create()
    {
        return view('clientes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'cedula' => [
                'required',
                'max:255',
                'min:5',
                Rule::unique('clientes')
            ],
            'nombre' => [
                'required',
            ],
            'telefono' => [
                'required',
                'max:255',
                'digits_between:11,20',
                Rule::unique('clientes'),
            ],
            'direccion' => [
                'required',
                'max:190',
            ],
        ], array(
            'required' => '* Campo requerido',
            'cedula.unique'=>'* Cédula ya registrada.',
            'cedula.min'=>'* Cédula require mín. 5 digitos',
            'telefono.unique'=>'* Teléfono ya registrado.',
            'telefono.digits_between'=>'* Require mín. 11 dígitos. Ejm: 02129999999'
        ));
        $cliente = $request->all();
        $message = 'Cliente creado con éxito';

        Cliente::create($cliente);
        if($cliente['pos'])
            return redirect('pos')->with('message', $message);
        else
            return redirect('clientes')->with('create_message', $message);
    }

    public function edit($id)
    {
        $cliente = Cliente::find($id);
        return view('clientes.edit', compact('cliente'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $this->validate($request, [
            'cedula' => [
                'required',
                'max:255',
                'min:5',
                Rule::unique('clientes')->ignore($id)
            ],
            'nombre' => [
                'required',
            ],
            'telefono' => [
                'required',
                'max:255',
                'digits_between:11,20',
                Rule::unique('clientes')->ignore($id)
            ],
            'direccion' => [
                'required',
                'max:190',
            ],
        ], array(
            'required' => '* Campo requerido',
            'cedula.unique'=>'* Cédula ya registrada.',
            'cedula.min'=>'* Cédula require mín. 5 digitos',
            'telefono.unique'=>'* Teléfono ya registrado.',
            'telefono.digits_between'=>'* Require mín. 11 dígitos. Ejm: 02129999999'
        ));


        $cliente = Cliente::find($id);
        $cliente->update($input);
        return redirect('clientes')->with('edit_message', 'Datos actualizados satisfactoriamente.');
    }

    public function destroy($id)
    {
        $cliente = Cliente::find($id);
        $cliente->save();
        return redirect('clientes')->with('not_permitted',trans('file.Data deleted successfully'));
    }
}
