<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Validator;
use Keygen;
use App\Cliente;
use App\Viajes;
use App\Reservaciones;
use Auth;
use DNS1D;

class ViajesController extends Controller
{
    public function index()
    {
        $viajes_all = Viajes::get();
        return view('viajes.index', compact('viajes_all'));
    }

    public function create()
    {
        return view('viajes.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        //dd($data);

        $rules = array(
            'codigo' => [
                'max:255',
                Rule::unique('viajes'),
            ],
        );

        $messages = array(
            'required' => '* Campo requerido',
            'codigo.unique' => '* Código ya registrado.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $failedRules = $validator->failed();
            return back()->withErrors($validator);
        } else {
            Viajes::create($data);
        }
        return redirect('viajes')->with('create_message', 'Viaje creado con éxito.');
    }

    public function edit($id)
    {
        $data = Viajes::where('id', $id)->first();

        return view('viajes.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'codigo' => [
                'max:255',
                Rule::unique('viajes')->ignore($id),
            ],
        );

        $messages = array(
            'required' => '* Campo requerido',
            'codigo.unique' => '* Código ya registrado.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $failedRules = $validator->failed();
            return back()->withErrors($validator);
        } else {
            $input = $request->all();

            $viajes = Viajes::find($id);
            $viajes->update($input);
        }
        return redirect('viajes')->with('edit_message', 'Viaje actualizado con éxito');
    }

    public static function show()
    {
        $id = Keygen::numeric(8)->generate();
        return $id;
    }


    public function destroy($id)
    {
        $data = Viajes::findOrFail($id);
        $data->delete();
        return redirect('viajes')->with('message', 'Viaje eliminado satisfactoriamente');
    }

    public function reservar()
    {
        $customer_list = Cliente::get();
        $viajes_list = Viajes::get();

        return view('viajes.reservar', compact('customer_list', 'viajes_list'));
    }

    public function getBooking($id)
    {
        $booking_data = Reservaciones::where(['viaje_id'=>$id])
                                        ->join('clientes', 'cliente_id','=','clientes.id')
                                        ->join('viajes', 'viaje_id','=','viajes.id')
                                        ->get(['reservaciones.id as reserva_id', 'clientes.*', 'viajes.*']);
        //dd($booking_data);

        return view('viajes.booking', compact('booking_data'));
    }

    public function getPlazas($id)
    {
        // obtener los datos del viaje
        $viaje = Viajes::where(['id'=>$id])->first();

        // obtener la cantidad de reservaciones hechas
        $booking = Reservaciones::where(['viaje_id'=>$id])->count();

        // calcular la cantidad de plazas restantes disponibles
        $disponibles = $viaje->num_plazas - $booking;

        echo $disponibles;
    }

    public function validateBooking(Request $request)
    {
        $data = $request->all();
        $viaje_id = $data['viaje_id'];
        $cliente_id = $data['cliente_id'];

        // verificar si el cliente ya se encuentra registrado en algun viaje
        $booking = Reservaciones::where(['viaje_id'=>$viaje_id, 'cliente_id'=>$cliente_id])->count();

        if ($booking>0) {
            $valid = 'N';
        }else {
            $valid = 'Y';
        }

        return response()->json( array(
            'valid' => $valid
        ));
    }

    public function booking(Request $request)
    {
        $data = $request->all();
        //dd($data);

        $this->validate($request, [
            'cliente_id' => [
                'required'
            ],
            'viaje_id' => [
                'required'
            ]
        ], array(
            'required' => '* Campo requerido'
        ));
        $data = $request->all();
        $message = 'Reservación agregada exitosamente.';

        Reservaciones::create($data);
        return redirect('reservaciones')->with('create_message', $message);
    }

    public function bookingDestroy($id)
    {
        $booking = Reservaciones::findOrFail($id);
        $booking->delete();
        return redirect('reservaciones')->with('message', 'Reservación eliminada satisfactoriamente');
    }

}
