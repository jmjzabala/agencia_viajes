 <?php $__env->startSection('content'); ?>
<?php if(session()->has('not_permitted')): ?>
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo e(session()->get('not_permitted')); ?></div>
<?php endif; ?>
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4><?php echo e(trans('file.add')); ?> <?php echo e(trans('file.customer')); ?></h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small><?php echo e(trans('file.The field labels marked with * are required input fields')); ?>.</small></p>
                        <?php echo Form::open(['route' => 'clientes.store', 'method' => 'post']); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo e(($errors->has('cedula'))?'error':''); ?>">
                                    <label><strong>Cédula *</strong> </label>
                                    <input type="text" name="cedula" required class="form-control">
                                    <?php if($errors->has('cedula')): ?>
                                    <label class="help-block"><?php echo e($errors->first('cedula')); ?></label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e(($errors->has('nombre'))?'error':''); ?>">
                                    <label><strong><?php echo e(trans('file.name')); ?> *</strong> </label>
                                    <input type="text" name="nombre" required class="form-control">
                                    <?php if($errors->has('nombre')): ?>
                                    <label class="help-block"><?php echo e($errors->first('nombre')); ?></label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e(($errors->has('telefono'))?'error':''); ?>">
                                    <label><strong><?php echo e(trans('file.Phone Number')); ?> *</strong></label>
                                    <input type="text" name="telefono" required class="form-control">
                                    <?php if($errors->has('telefono')): ?>
                                    <label class="help-block"><?php echo e($errors->first('telefono')); ?></label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php echo e(($errors->has('direccion'))?'error':''); ?>">
                                    <label><strong><?php echo e(trans('file.Address')); ?> *</strong></label>
                                    <input type="text" name="direccion" required class="form-control">
                                    <?php if($errors->has('direccion')): ?>
                                    <label class="help-block"><?php echo e($errors->first('direccion')); ?></label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="hidden" name="pos" value="0">
                                    <input type="submit" value="<?php echo e(trans('file.submit')); ?>" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>