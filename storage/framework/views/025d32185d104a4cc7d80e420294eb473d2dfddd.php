<thead>
    <tr>
        <th>Cédula</th>
        <th><?php echo e(trans('file.name')); ?></th>
        <th><?php echo e(trans('file.Phone Number')); ?></th>
        <th><?php echo e(trans('file.viaje')); ?></th>
        <th><?php echo e(trans('file.Price')); ?></th>
        <th><?php echo e(trans('file.Subtotal')); ?></th>
        <th><i class="fa fa-trash"></i></th>
    </tr>
</thead>
<tbody>
<?php $total=0; foreach ($booking_data as $booking) { ?>
    <tr>
        <td><?php echo e($booking->cedula); ?></td>
        <td><?php echo e($booking->nombre); ?></td>
        <td><?php echo e($booking->telefono); ?></td>
        <td><?php echo e($booking->origen.' - '.$booking->destino); ?></td>
        <td><?php echo e($general_setting->currency); ?> <?php echo e(number_format($booking->precio,2,',','.')); ?></td>
        <td><?php echo e($general_setting->currency); ?> <?php echo e(number_format($booking->precio,2,',','.')); ?></td>
        <td><a href="<?php echo e(route('reservacion.destroy', ['id' => $booking->reserva_id])); ?>" onclick="return confirm('¿Seguro desea eliminar esta reservación?');"><i class="fa fa-trash"></i></a></td>
        <?php $total += $booking->precio; ?>
    </tr>
<?php } ?>
</tbody>
<tfoot class="tfoot active">
    <th colspan="5"><?php echo e(trans('file.Total')); ?></th>
    <th id="total"><?php echo e($general_setting->currency); ?> <?php echo e(number_format($total,2,',','.')); ?></th>
    <th><i class="fa fa-trash"></i></th>
</tfoot>