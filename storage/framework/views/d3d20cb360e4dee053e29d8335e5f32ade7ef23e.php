 <?php $__env->startSection('content'); ?>
<?php if(session()->has('create_message')): ?>
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo e(session()->get('create_message')); ?></div>
<?php endif; ?>
<?php if(session()->has('edit_message')): ?>
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo e(session()->get('edit_message')); ?></div>
<?php endif; ?>
<?php if(session()->has('import_message')): ?>
    <div class="alert alert-success alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo e(session()->get('import_message')); ?></div>
<?php endif; ?>
<?php if(session()->has('not_permitted')): ?>
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo e(session()->get('not_permitted')); ?></div>
<?php endif; ?>
<?php if(session()->has('message')): ?>
  <div class="alert alert-danger alert-dismissible text-center"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><?php echo e(session()->get('message')); ?></div>
<?php endif; ?>

<section>
    <div class="container-fluid">
        <a href="<?php echo e(route('viajes.create')); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Agregar Viaje</a>
    </div>
    <div class="table-responsive">
        <table id="product-data-table" class="table table-hover">
            <thead>
                <tr>
                    <th class="not-exported"></th>
                    <th><?php echo e(trans('file.Code')); ?></th>
                    <th>Núm. Plazas</th>
                    <th>Destino</th>
                    <th>Origen</th>
                    <th><?php echo e(trans('file.Price')); ?></th>
                    <th class="not-exported"><?php echo e(trans('file.action')); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $viajes_all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $viaje): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr class="viaje-link" data-viaje='[ "<?php echo e($viaje->id); ?>", "<?php echo e($viaje->codigo); ?>", "<?php echo e($viaje->num_plazas); ?>", "<?php echo e($viaje->destino); ?>", "<?php echo e($viaje->origen); ?>", "<?php echo e($general_setting->currency); ?> <?php echo e($viaje->precio); ?>"]'>
                    <td><?php echo e($key); ?></td>
                    <td><?php echo e($viaje->codigo); ?></td>
                    <td><?php echo e($viaje->num_plazas); ?></td>
                    <td><?php echo e($viaje->destino); ?></td>
                    <td><?php echo e($viaje->origen); ?></td>
                    <td><?php echo e($general_setting->currency); ?> <?php echo e(number_format($viaje->precio,2,',','.')); ?></td>
                    <td>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo e(trans('file.action')); ?>

                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu edit-options dropdown-menu-right dropdown-default" user="menu">
                              <li>
                                <a href="<?php echo e(route('viajes.edit', ['id' => $viaje->id])); ?>" class="btn btn-link"><i class="fa fa-edit"></i> <?php echo e(trans('file.edit')); ?></a>
                              </li>
                                <li class="divider"></li>
                                <?php echo e(Form::open(['route' => ['viajes.destroy', $viaje->id], 'method' => 'DELETE'] )); ?>

                                <li>
                                    <button type="submit" class="btn btn-link" onclick="return confirmDelete()"><i class="fa fa-trash"></i> <?php echo e(trans('file.delete')); ?></button>
                                </li>
                                <?php echo e(Form::close()); ?>

                            </ul>
                        </div>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</section>

<script type="text/javascript">

	function confirmDelete() {
	    if (confirm("¿Seguro quiere eliminar este viaje?")) {
	        return true;
	    }
	    return false;
	}

    $('#product-data-table').DataTable( {
        "order": [],
        "language": {
            "url": "<?php echo e(url('public/vendor/datatable/dataTables.spanish.json')); ?>"
        },
        'columnDefs': [
            {
                "orderable": false,
                'targets': [0, 6]
            },
            {
                'checkboxes': {
                   'selectRow': true
                },
                'targets': 0
            }
        ],
        'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        dom: 'lftipr'
    } );

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>