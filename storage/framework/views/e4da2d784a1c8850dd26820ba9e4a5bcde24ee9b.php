<h1><?php echo e(trans('file.Sale')); ?> <?php echo e(trans('file.Details')); ?></h1>
<p><strong><?php echo e(trans('file.Reference')); ?>: </strong><?php echo e($reference_no); ?></p>
<p>
	<strong><?php echo e(trans('file.Status')); ?> <?php echo e(trans('file.Sale')); ?>: </strong>
	<?php if($sale_status==1): ?>{trans('file.completed')}
	<?php elseif($sale_status==2): ?>{trans('file.pending')}
	<?php endif; ?>
</p>
<p>
	<strong><?php echo e(trans('file.Status')); ?> <?php echo e(trans('file.Payment')); ?>: </strong>
	<?php if($payment_status==1): ?>{trans('file.pending')}
	<?php elseif($payment_status==2): ?>{trans('file.due')}
	<?php elseif($payment_status==3): ?>{trans('file.partial')}
	<?php else: ?>{trans('file.paid')}<?php endif; ?>
</p>
<h3><?php echo e(trans('file.Order Table')); ?></h3>
<table style="border-collapse: collapse; width: 100%;">
	<thead>
		<th style="border: 1px solid #000; padding: 5px">#</th>
		<th style="border: 1px solid #000; padding: 5px"><?php echo e(trans('file.product')); ?></th>
		<th style="border: 1px solid #000; padding: 5px"><?php echo e(trans('file.Download Link')); ?></th>
		<th style="border: 1px solid #000; padding: 5px"><?php echo e(trans('file.qty')); ?></th>
		<th style="border: 1px solid #000; padding: 5px"><?php echo e(trans('file.Unit Price')); ?></th>
		<th style="border: 1px solid #000; padding: 5px"><?php echo e(trans('file.Subtotal')); ?></th>
	</thead>
	<tbody>
		<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($key+1); ?></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($product); ?></td>
			<?php if($file[$key]): ?>
				<td style="border: 1px solid #000; padding: 5px"><a href="<?php echo e($file[$key]); ?>">Download</a></td>
			<?php else: ?>
				<td style="border: 1px solid #000; padding: 5px">N/A</td>
			<?php endif; ?>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($qty[$key].' '.$unit[$key]); ?></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e(number_format((float)($total[$key] / $qty[$key]), 2, ',', '.')); ?></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($total[$key]); ?></td>
		</tr>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		<tr>
			<td colspan="3" style="border: 1px solid #000; padding: 5px"><strong>Total </strong></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($total_qty); ?></td>
			<td style="border: 1px solid #000; padding: 5px"></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($total_price); ?></td>
		</tr>
		<tr>
			<td colspan="5" style="border: 1px solid #000; padding: 5px"><strong><?php echo e(trans('file.Order Tax')); ?> </strong> </td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($order_tax.'('.$order_tax_rate.'%)'); ?></td>
		</tr>
		<tr>
			<td colspan="5" style="border: 1px solid #000; padding: 5px"><strong><?php echo e(trans('file.Order discount')); ?> </strong> </td>
			<td style="border: 1px solid #000; padding: 5px">
				<?php if($order_discount): ?><?php echo e($order_discount); ?>

				<?php else: ?> 0 <?php endif; ?>
			</td>
		</tr>
		<tr>
			<td colspan="5" style="border: 1px solid #000; padding: 5px"><strong><?php echo e(trans('file.Shipping Cost')); ?></strong> </td>
			<td style="border: 1px solid #000; padding: 5px">
				<?php if($shipping_cost): ?><?php echo e($shipping_cost); ?>

				<?php else: ?> 0 <?php endif; ?>
			</td>
		</tr>
		<tr>
			<td colspan="5" style="border: 1px solid #000; padding: 5px"><strong><?php echo e(trans('file.Grand Total')); ?></strong></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e($grand_total); ?></td>
		</tr>
		<tr>
			<td colspan="5" style="border: 1px solid #000; padding: 5px"><strong><?php echo e(trans('file.Amount')); ?> <?php echo e(trans('file.Paid')); ?></strong></td>
			<td style="border: 1px solid #000; padding: 5px">
				<?php if($order_discount): ?><?php echo e($paid_amount); ?>

				<?php else: ?> 0 <?php endif; ?>
			</td>
		</tr>
		<tr>
			<td colspan="5" style="border: 1px solid #000; padding: 5px"><strong>Balance</strong></td>
			<td style="border: 1px solid #000; padding: 5px"><?php echo e(number_format((float)($grand_total - $paid_amount), 2, '.', '')); ?></td>
		</tr>
	</tbody>
</table>

<p><?php echo e(trans('file.Thank You')); ?></p>