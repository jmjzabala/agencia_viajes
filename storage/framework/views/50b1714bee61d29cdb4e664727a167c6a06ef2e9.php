<?php $__env->startSection('content'); ?>
<section class="forms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h4><?php echo e(trans('file.update')); ?> <?php echo e(trans('file.viaje')); ?></h4>
                    </div>
                    <div class="card-body">
                        <p class="italic"><small><?php echo e(trans('file.The field labels marked with * are required input fields')); ?>.</small></p>
                        <?php echo Form::open(['route' => ['viajes.update', $data->id], 'method' => 'put', 'id' => 'viaje-form']); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php echo e(($errors->has('codigo'))?'error':''); ?>">
                                    <label><strong><?php echo e(trans('file.viaje')); ?> <?php echo e(trans('file.Code')); ?> *</strong> </label>
                                    <div class="input-group">
                                        <input type="text" name="codigo" required class="form-control" value="<?php echo e($data->codigo); ?>">
                                        <div class="input-group-append">
                                            <button id="genbutton" type="button" class="btn btn-default"><?php echo e(trans('file.Generate')); ?></button>
                                        </div>
                                    </div>
                                    <?php if($errors->has('codigo')): ?>
                                    <label class="help-block"><?php echo e($errors->first('codigo')); ?>

                                    </label>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label><strong>Núm. plazas *</strong> </label>
                                    <input type="number" name="num_plazas" required class="form-control" min="1" step="any" value="<?php echo e($data->num_plazas); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label><strong>Destino *</strong> </label>
                                    <input type="text" name="destino" required class="form-control" value="<?php echo e($data->destino); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-group">
                                    <label><strong>Origen *</strong> </label>
                                    <input type="text" name="origen" required class="form-control" value="<?php echo e($data->origen); ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><strong><?php echo e(trans('file.Price')); ?> *</strong> </label>
                                    <input type="number" name="precio" required class="form-control" step="any" value="<?php echo e($data->precio); ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="<?php echo e(trans('file.submit')); ?>" class="btn btn-primary">
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#genbutton').on("click", function(){
      $.get('../gencode', function(data){
        $("input[name='codigo']").val(data);
      });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>